Use quick tile.
```bash
sudo pip3 install https://github.com/ssokolow/quicktile/archive/master.zip
quicktile --show-bindings

```

```bash
# Backup
gsettings list-recursively org.mate.Marco.window-keybindings >~/pimp-my-shell/linux/mate/key-bindings/gsettings

# restore
# Pipe this through bash
cat ~/pimp-my-shell/linux/mate/key-bindings/gsettings |sed 's/^/gsettings set /'
```
