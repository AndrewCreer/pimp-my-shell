#OSX setup

```bash
brew install rsnapshot
brew install rsync
rsnapshot -c ~/pimp-my-shell/rsnapshot/rsnapshot.conf daily
crontab ~/pimp-my-shell/rsnapshot/crontab
crontab -l
```

#Linux setup

```bash
sudo apt install rsnapshot
mkdir ~/rsnapshot
rsnapshot -c ~/pimp-my-shell/rsnapshot/rsnapshot-$(uname).conf daily
crontab ~/pimp-my-shell/rsnapshot/crontab.$(uname)
crontab -l
```
