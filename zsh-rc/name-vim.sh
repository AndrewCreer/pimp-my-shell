function namevim(){ 
	gvim --servername $1 --remote-silent "${@:2}"
}

alias ggvim='namevim RTL'

# no coc stuff for logs
alias lvim='gvim --servername LOG -u ~/pimp-my-shell/vim/vimrc --remote-silent'
alias pvim='namevim PIMP'
alias uvim='namevim UNI'
alias lsvim='vim --serverlist'
