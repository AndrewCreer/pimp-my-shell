function tiltup() { 
	existingPid=$(ps awx |grep "tilt up"|grep -v grep| sed 's/^\s*//' |cut -d' ' -f1)
	if [[ -z $existingPid ]]
	then
		if [[ -z "$@" ]];then
			hi |grep ^tiltup\\s. |uniq
			echo "\nTilt not running but no args supplied"
		else
			tilt up $@ --stream > /tmp/tilt.log  &
			disown
			lvim /tmp/tilt.log 
		fi
	else
		hi |grep ^tiltup\\s. |uniq
		echo "\nTilt is running in $existingPid"
		if [[ -z "$@" ]]; then
			echo "kill $existingPid; tiltup $@"
		else
			echo "kill $existingPid"
		fi

	fi
}

function tiltlog(){
	for i in "${@[@]}"
	do
		existingPid=$(ps awx |grep "tilt logs $i "|grep -v grep|cut -d' ' -f1 )
		if [[ -z "$existingPid" ]]
		then
			tilt logs "$i" --follow  > /tmp/$i.log &
			disown
			lvim /tmp/$i.log
		else
			# echo "$i is already logged in $existingPid"
			# echo "kill $existingPid # kill $i logging "
		fi
	done
	ps awx |grep 'tilt logs' |grep -v grep
	echo "ps awx |grep 'tilt logs' |grep -v grep|sed 's/^\s*/ /g'|cut -d' ' -f2 | xargs kill # Kill all logs"
}

# Added by mono-repo werf/trdl install.
[[ "$PATH" == *"$HOME/bin:"* ]] || export PATH="$HOME/bin:$PATH"

! { which werf | grep -qsE "^/home/andrew/.trdl/"; } && [[ -x "$HOME/bin/trdl" ]] && source $("$HOME/bin/trdl" use werf "1.2" "stable")

function eeAuth(){
	local eeEnv=$1
	export EES_AUTH_CLIENT_SECRET=$(./support/scripts/print-secrets.sh service/eagle-eye $eeEnv | yq .secrets.EAGLE_EYE_API_SECRET) ;
	export EES_AUTH_CLIENT_ID=$(./support/scripts/print-secrets.sh -v service/eagle-eye $eeEnv |yq .env.EE_CLIENT_ID) ;
	export EE_WALLET_URL=$(support/scripts/print-secrets.sh       -v service/eagle-eye $eeEnv |yq .env.EE_WALLET_URL)
	export EE_POS_URL=$(support/scripts/print-secrets.sh          -v service/eagle-eye $eeEnv |yq .env.EE_POS_URL)
	export EE_RESOURCES_URL=$(support/scripts/print-secrets.sh    -v service/eagle-eye $eeEnv |yq .env.EE_RESOURCES_URL)
	export EES_ENV=$eeEnv
	env |grep ^EE
}

alias eedev='eeAuth dev'
alias eetest='eeAuth test'
alias eeuat='eeAuth uat'

alias eebase='export EE_BASE=../samples/$(git branch --show-current)/ee-data/$EES_ENV;echo $EE_BASE'
alias eebaselcn='export EE_BASE=../samples/$(git branch --show-current)/ee-data/$EES_ENV/$MYLCN;echo $EE_BASE'
alias eeresults='find ../samples/$(git branch --show-current) -name response.json |sort |sed "s/^/cat /"'

export DATABASE_URL=$(yq .secrets.DATABASE_URL ~/woolies/rtl/service/canonical/.helm/local/secrets.yaml|sed 's/@postgres/@localhost/')
export DATABASE_URL_ALLOYDB=$DATABASE_URL

function v2trlocal(){
	curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_transaction_v2_secret_local http://localhost:3010/$@
}
function v2trtest(){ curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_transaction_v2_secret_test https://transaction-v2-rtl-test.gcp-wx-d.net/$@ }
function v2truat(){ curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_transaction_v2_secret_uat https://transaction-v2-rtl-uat.gcp-wx-d.net/$@ }

function v2local(){
	curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_activity_v2_secret_local http://localhost:3008/$@
}
function v2test(){ curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_activity_v2_secret_test https://activity-v2-rtl-test.gcp-wx-d.net/$@ }
function v2uat(){ curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_activity_v2_secret_uat https://activity-v2-rtl-uat.gcp-wx-d.net/$@ }

function bptest(){ curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_bp_nz_secret_test https://bp-nz-rtl-test.gcp-wx-d.net/$@ }

function custlocal(){ curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_customer_secret_local http://localhost:3011/$@ }
function custtest(){ curl -D /dev/stderr -H $cth -H x-api-key:$rtl_api_customer_secret_test https://customer-rtl-test.gcp-wx-d.net/$@ }

function cardlocal(){
	curl -sD /dev/stderr -H $cth -u $card_usr_local:$card_pwd_local http://localhost:3001/$@
}

function cardtest(){
	curl -D /dev/stderr -H $cth -u $card_usr_test:$card_pwd_test https://card-rtl-test.gcp-wx-d.net/$@ 
}
function trtest(){curl -D /dev/stderr -H $cth -u $transaction_usr_test:$transaction_pwd_test https://transaction-rtl-test.gcp-wx-d.net/$@ }
function trlocal(){curl -D /dev/stderr -H $cth -u $transaction_usr_local:$transaction_pwd_local http://localhost:3003/$@ }

function convtest(){curl -sD /dev/stderr -H $cth -H x-api-key:$rtl_api_conversion_secret_test https://conversion-rtl-test.gcp-wx-d.net/$@}

export LOCAL_RTL_CLUSTER_NAME=kind-kind

function patchTestConfig(){
	local dashes=$1
	# echo "dashes '$dashes'"
	camel=$(echo "$dashes" |sed 's/-\(\w\)/\U\1/g')
	# echo "$camel"

	git apply <<EOF
diff --git a/test/acceptance-jest/src/config/test.ts b/test/acceptance-jest/src/config/test.ts
index 7408b99c8..1d7c559a3 100644
--- a/test/acceptance-jest/src/config/test.ts
+++ b/test/acceptance-jest/src/config/test.ts
@@ -1,5 +1,6 @@
 import type {EnvironmentConfig} from ".";
 import {appEnvVars} from "../runtime/appEnvVars";
+import {localClusterUrl} from "../runtime/localClusterUrl";
 
 export const testConfig = (): EnvironmentConfig => ({
 	apis: {

EOF


sed -i "s/^.*$dashes.*$/baseUrl:localClusterUrl('$dashes'),/" test/acceptance-jest/src/config/test.ts

sed -i "s/^.*\.$camel.*$/key:'wxrtl_acceptance_test',/" test/acceptance-jest/src/config/test.ts

npx prettier -w test/acceptance-jest/src/config/test.ts

}

