# brew install colordiff
alias diff='colordiff -u'

# Everyone needs a personal ~/bin in their path. 
[ -d ~/bin ] || mkdir  ~/bin
[[ "$PATH" == *"$HOME/bin:"* ]] || export PATH="$HOME/bin:$PATH"

# Some mac things are good.  sudo apt install xclip xsel
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
alias pb='xclip -o'

# Brew on linux https://brew.sh/
[ -f /home/linuxbrew/.linuxbrew/bin/brew ] && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# less should not page for short output, allow color
# set tabs as 4 spaces with -x1,5
export LESS=FXRx1,5

# https://unix.stackexchange.com/questions/693118/trim-trailing-newline-from-pasted-text-in-zsh
bracketed-paste() {
  zle .$WIDGET && LBUFFER=${LBUFFER%$'\n'}
}
zle -N bracketed-paste

# Ruby stuff
export GEM_HOME=$HOME/.gem
export PATH="$GEM_HOME/bin:$PATH"

# pip install python stuff ends here 
export PATH="$HOME/.local/bin:$PATH"

# /usr/share/doc/fzf/README.Debian
[ -f /usr/share/doc/fzf/examples/key-bindings.zsh ] && source /usr/share/doc/fzf/examples/key-bindings.zsh

# Create branch names from jira tickets.
function jiracurl(){
	curl -su acreer1@woolworths.com.au:$(cat ~/woolies/jira-token) \
		-H content-type:application/json https://woolworthsdigital.atlassian.net/rest/api/3/search\?jql\=$(
			# Paste into jira 'advanced search' to edit/check the query
			# assignee = currentUser() AND issuetype != Sub-task AND status != Done AND sprint in opensprints() ORDER BY priority ASC
			echo '
			assignee = currentUser() AND status != Done ORDER by sprint,priority DESC
			' \
			 |jq -sRr @uri
		) \
	| jq -r '.issues[]|[.fields.issuetype.name,.key,.fields.summary|ascii_downcase]|@tsv' \
	| sed 's/[][&:/()]//g;s/build//;s/test//;s/[[:space:]]\+/-/g;s#^story-#feature/#;s#bug-#feature/fix-#;s/---/-/g'
	# Sed changes
  #   story	og-2665	[build/test]: clcs fv balances migration (accrual/redemption status)
  # into
	#   feature/og-2665-clcs-fv-balances-migration-accrualredemption-status
}

# create branch names from a selection, after they turn off access to the jira api.
function jirapaste(){
	branch=$(
		pb | \
		sed '1d;/^[0-9]$/d;/^\W$/d;s/$/ /' | \
		tr '[:upper:]' '[:lower:]' | \
		tr -d '\n'| \
		sed 's/[][&:/()]//g;s/build//;s/test//;s/[[:space:]]\+/-/g;s#^#feature/#;s/--/-/g;s/-$/\n/'
	)
	echo "gcb $branch"
	echo "gco $branch"
}

# See whats happening in your repo.  commands to checkout branches.
# sudo apt install gh
function ghprlist(){
	gh pr list --limit 200 --json id,title,author,headRefName,number,isDraft,comments | \
		jq -r '.[]|[.number,.headRefName,.author.login,.title,if .isDraft then "Draft" else "Review" end,(.comments|length)]|@tsv'| \
		sed 's/\s/ # /;s/^/gh co /'
}

alias coltab="column -ts $'\t'"

# curl -H $cth is shorter
export cth='content-type:application/json'

# might seem like a good idea...   sudo apt --purge autoremove
alias apu='echo "apt update etc..." && sudo apt update && sudo apt list --upgradable && sudo apt upgrade && sudo apt install cryptsetup cryptsetup-initramfs cryptsetup-bin && snap refresh --list && sudo snap refresh'

# https://github.com/jqlang/jq/issues/78 How to dump paths to leafs and the leaf values?
alias jqs="jq -c 'paths(scalars) as \$p|\$p+[getpath(\$p)]'"

function jt(){
			echo "RUNNING Jest Tests with a tag of $@"
        tmplr
        JEST_TAGS=$@ npm run acceptance-test -- -d |& tee /tmp/acceptance.log
}

alias fd=fdfind
alias bqj='bq query --use_legacy_sql=false --format json'
