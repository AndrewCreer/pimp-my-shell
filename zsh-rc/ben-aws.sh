
### Pre-requisites
# JFROG_URL_DOCKER=docker.repo.bab.net.au
# brew install jq

#### 
# 
# NOTES
#
# All functions are 
#   aws- 
# prefixed.  Anticipating 
#   gcp- 
# versions for google cloud platform
#
# Login to aws, and choose an account (prod, non-prod etc) optionally accepts
# the name of a cluster that is you know you will choose after you login.
# 
# Copied from Jay https://confluence.bbl.int/display/PLT/Kube+-+Logging+into+AWS+Cluster
# 
#####

function aws-auth {
  # clear existing auth.
  rm -rf ~/.aws
  # Get credentials for AWS accounts and assume an IAM role
  docker run -it --rm -e username="${LOGNAME}@bendigoadelaide.com.au" \
      -v ${HOME}/.aws:/root/.aws -w /work $JFROG_URL_DOCKER/ben-aws-adfs:1.24.3 \
      login --adfs-host=adfs.bendigoadelaide.com.au --region=ap-southeast-2 --env
  echo "*** Updating all EKS clusters credentials ***"
  
  aws-update_kubeconfig $1
}

# After you are logged into a perticular account, you can see the eks clusters
# defined.  This is the 'help' if you dont supply an argument to aws-update_kubeconfig
function aws-list-clusters(){
  echo "Run these commands for each cluster"
  for c in `aws eks list-clusters |jq -r '.clusters[]'`
  do
    echo "aws-update_kubeconfig $c"
  done
}

function aws-update_kubeconfig(){
  local CLUSTER_NAME=$1

  if [ -z $CLUSTER_NAME ]
  then
    # User Help. Print list of commands for copy paste..
    aws-list-clusters
  else
    docker run -it --rm \
    -v ${HOME}/.kube:/root/.kube \
    -v ${HOME}/.aws:/root/.aws \
    --entrypoint "" \
    $JFROG_URL_DOCKER/aws-kubectl-helm:latest \
    aws eks --region ap-southeast-2 update-kubeconfig --name ${CLUSTER_NAME}
  fi
}

# Run abitrary aws commands.
# LIKE 
#   aws ssm get-parameter --name "${SSM_API_SECRET_NAME}" --query Parameter.Value
function benaws(){
    docker run -it --rm \
    -v ${HOME}/.kube:/root/.kube \
    -v ${HOME}/.aws:/root/.aws \
    --entrypoint "" \
    $JFROG_URL_DOCKER/aws-kubectl-helm:latest \
    aws $@
}


