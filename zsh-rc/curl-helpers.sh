#####################
#
# I use curl instead of postman because its better.
#
# I want to record every curl I've ever made, so I can look back and see what I
# did, and when..
#
# I have 4 main use cases, so 4 curl commands.
#
# 1. hcurl
#  - stores the replay File the params in a file
#  - uses tee to save the output and then put it on STDOUT (man tee)
#  - adds in the basename of the folder you are in.
#
# 2. pcurl
#  - Adds a content-type header, calls history
#
# 3. lcurl to a locally running api
#  - calls pcurl
#  - makes your curl look like its come through apiConnect to your api
#  - adds x-api-app-id header
#  - adds x-global-transaction-id header
#  - adds x-api-secret header
#
# 4. ccurl client/secret
#  - Adds client-secret headers.
#
###########

CURL_HISTORY_FOLDER=~/curl_history

# Make the CURL_HISTORY_FOLDER
[ -d "$CURL_HISTORY_FOLDER" ]  || mkdir $CURL_HISTORY_FOLDER

function hcurl(){
  local now=$(date +%F--%X)
  local nowFolder=$CURL_HISTORY_FOLDER/$now
  mkdir -p $nowFolder
  local replayFile=$nowFolder/1-replay.sh
  echo curl -sD /dev/stderr $@ >$replayFile
  echo curl -sD /dev/stderr $@ | sed 's/secret:[^ ]*/secret:REDACTED/'|perl -pe 's/wxrtl_.*?\b/wx_rtl_REDACTED/' >$nowFolder/1-replay.redact
  chmod +x $replayFile
  local outputFile=$nowFolder/2-output.json

  curl -sD $nowFolder/headers $@ | tee $outputFile
}

function bcurl(){
  hcurl -sH content-type:application/json -u $BASIC_AUTH_USER:$BASIC_AUTH_PASS $@
}

function kcurl(){
  hcurl -sH content-type:application/json -H api-key:$API_KEY $@
}

# function lcurl(){
#   local gbl=$USER-$(date +%F--%X')
#   pcurl -H x-apic-secret:$(pcre2grep -o1 'APIC_SECRET=(.*)' .env) -H x-api-app-id:${DEFAULT_CHANNEL:-Alt} -H x-global-transaction-id:$gbl $@
# }

# function ccurl(){
#   # TODO interogate URL and look for prod/dev and source the correct file.
#   if [ -z $CLIENT_SECRET ]
#   then
#     echo "AUTO SOURCING ~/projects/everest-identity/up-dev-dev.sh"
#     source ~/projects/everest-identity/up-dev-dev.sh
#     # local CLIENT_SECRET=$(grep CLIENT_SECRET .env|cut -d= -f2)
#   fi
#   if [ -z $CLIENT_ID ]
#   then
#     # local CLIENT_ID=$(grep CLIENT_ID .env|cut -d= -f2)
#   fi
#   pcurl -H x-ibm-client-secret:$CLIENT_SECRET -H x-ibm-client-id:$CLIENT_ID $@
# }

alias hhcurl='find $CURL_HISTORY_FOLDER |sort |sed "s/^/cat /"'
