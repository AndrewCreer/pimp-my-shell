# redirect common (?) commands to files, and open them in vim (or code)
# brew install entr 
# brew install fd
# brew install macvim

function apidev(){
  local dir=`basename $PWD`
  local logFile=/tmp/$dir.api.log
  echo logFile=$logFile
  touch $logFile

  gvim --servername ${dir}fwdev --remote-silent $logFile
  npm run dev </dev/null |& tee $logFile  &
}

function testdev(){
  set -x
  local dir=`basename $PWD`
  local logFile=/tmp/$dir.test.log
  
  touch $logFile
  gvim --servername ${dir}Test --remote-silent $logFile &

  local cmd="'npm run build && npx mocha --grep $@  &> $logfile'"
  echo $cmd
  fdfind |entr -cs "'$cmd'"
  # fd |entr -s "yarn test $@ |& tee $logFile"

}

