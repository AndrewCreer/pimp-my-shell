# Permanent infinite history
# OSX needs 'real' versions of mkdir.
#  install versions with g infront to that your existing stuff still works.
# brew install coreutils
#
# Makes the history folder
# OSX [ -d ~/.zsh_history.d ] || gmkdir --mode=0700 ~/.zsh_history.d
[ -d ~/.zsh_history.d ] || mkdir --mode=0700 ~/.zsh_history.d
#
# Makes each shell get its own history file with unix epoch in the filename
HISTFILE=~/.zsh_history.d/zsh_history.$(date +%s)
#
# Update history after each command (rather than at logout)
setopt INC_APPEND_HISTORY
#
# Everything I've ever typed
# OSX alias hi='gcat $(find ~/.zsh_history.d -type f|sort )|gcut -d";" -f2-'
alias hi='cat $(find ~/.zsh_history.d -type f|sort   )|cut -d";" -f2-'
alias hr='print -s $(tac $(find ~/.zsh_history.d -type f|sort -r)|cut -d";" -f2-|fzf --no-sort)'
#
# Just the things in files with a cd into the same folder you are now..
alias hid='cat $(grep -lr ";cd.*$(basename `pwd`)" ~/.zsh_history.d)|cut -d";" -f2-'

# Just history for this shell
alias hh=history
alias hhi='history |sed -E "s/^[[:blank:]]+[[:digit:]]+[[:blank:]]+//"'
