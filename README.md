# pimp-my-shell

Some things that make my life easier

## LINUX

```zsh
sudo apt install zsh
chsh -s $(which zsh)

# logout


# New install
sudo apt install git

# get chrome
sudo apt-get install libxss1 libappindicator1 libindicator7
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb
rm $!



git clone git@gitlab.com:AndrewCreer/pimp-my-shell.git
# fix .vimrc and .zshrc, as per below
sudo apt install xclip

sudo apt install zsh
sudo apt install curl
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# might not be needed
chsh /usr/bin/zsh

sudo apt install colorized-logs # provides ansi2txt
sudo apt install gridsite-clients # for urlencode

sudo apt install fd-find thunderbird
sudo apt install fzf

# Make caps lock into another escape
gsettings set org.mate.peripherals-keyboard-xkb.kbd options "['caps\tcaps:escape']"

# Fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/Hack.zip -P ~/Downloads
mkdir ~/.local/share/fonts
unzip  ~/Downloads/Hack.zip -d ~/.local/share/fonts

# Set font in mate terminal (use gui to edit default profile) Needs a new term to see if cli used.
dconf write /org/mate/terminal/profiles/default/font "''FiraCode Nerd Font Propo Medium 9''"

sudo apt install bruno
sudo apt install ripgrep
sudo apt install colordiff
sudo apt install libvips libvips-dev libvips-tools
sudo apt install lftp

# https://cloud.google.com/sdk/docs/install#deb
sudo apt-get install apt-transport-https ca-certificates gnupg curl
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
sudo apt update && sudo apt install google-cloud-cli
sudo apt install google-cloud-sdk-gke-gcloud-auth-plugin
```

## MAC

```bash
# GET 'modern' gnu/linux versions of sed etc that 'work'
# The mac defaults are older bsd versions
# Use gsed and gmkdir to use the new versions,
brew install coreutils
brew install gnu-sed

# maybe I should use jq, but this is good
yarn global add json

# be a cool kid
https://github.com/ohmyzsh/ohmyzsh

# Dad jokes for Seb
alias dj='curl -H "Accept: text/plain" https://icanhazdadjoke.com/'

# Better versions of grep and find ignore .gitignored things like node_modules
brew install fd
man fd
brew install ripgrep
man rg

# https://github.com/junegunn/fzf
# CLI command line fuzzy matching.  Also has vim plugin
brew install fzf
man fzf

```

# ZSH Helper Functions.

At the end of my `~/.zshrc` I have

```
# Load some things from my source controlled shared
pimps=(
  curl-helpers
  infitite-history
  good-stuff
  name-vim
  woolies
)
for pl in "$pimps[@]"
do
  fn=~/pimp-my-shell/zsh-rc/$pl.sh
  echo "pimp-my-shell: loading fn=$fn"
  [ -f $fn ] && source $fn
done

[ -d $HOME/pimp-my-shell/bin ] && export PATH="$HOME/pimp-my-shell/bin:$PATH"

```

# VIM

my `~/.vimrc` file

```vim
source ~/pimp-my-shell/vim/vimrc
source ~/pimp-my-shell/vim/vimrc.coc
```

# Swapspce.

Make a 20G? swapfile for 16G of ram.
If you choose the standard encrypted ubuntu and end up with a 2G swap partition

```
sudo dd if=/dev/zero of=/swapfile bs=1G count=20
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

swapon --show

# Make it happen on each boot
echo "/swapfile none swap sw 0 0" |sudo tee -a /etc/fstab
```
